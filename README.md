# Projeto 1 - 2020/2

## Compilação

Para compilar, digite o comando abaixo dentro do diretório do projeto:

`$ make`

## Execução

Para executar o programa, digite o comando abaixo:

`$ make run` 

## Utilização

Ao executar o programa, será apresentado a cada **1 segundo** as temperaturas medidas.

> **TE** = Temperatura Externa (ambiente) <br>
> **TI** = Temperatura Interna <br>
> **TR** = Temperatura de Referência (definida pelo potenciômetro ou pelo usuário) <br>

A princípio o programa começa com o TR definido pelo potênciometro, para mudar a temperatura simplesmente digite o **valor** dela (ex. 55.88). <br>
Caso deseje que o potênciometro volte a definir a temperatura, digite **0**.

### Exemplo
![exemplo](https://i.imgur.com/HwGASO4.png)

**OBS:** Será desconsiderado se a temperatura definida pelo usuário for menor do que a temperatura externa.

## Gráficos
Abaixo segue os gráficos gerados num período de **10 minutos** com a variação da temperatura de referência definida pelo **potenciômetro**.

![temp_time](https://i.imgur.com/LvnRl7P.png)

![pid_time](https://i.imgur.com/pQcdQH2.png)
