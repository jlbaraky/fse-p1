#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include "uart.h"
#include "lcd.h"
#include "bme280.h"
#include "pwm.h"
#include "pid.h"

#define CUSTOM 1
#define POTENCIOMETER 0

FILE *logger;
float ti, tr, te, tc /*custom temperature*/;
int type = POTENCIOMETER;
int count = 0;

void *input() {
    float temperature;
    while(1) {
        scanf(" %f", &temperature);
        if (temperature == 0.0) {
            type = POTENCIOMETER;
            printf("\n> Temperatura de refêrencia sendo definida pelo potênciometro.\n\n");
	}
        else if (temperature > te)  {
            tc = temperature;
            type = CUSTOM;
            printf("\n> Temperatura de refêrencia definida como %.2f ºC.\n\n", tc);
        } else
            printf("\n> Erro: Temperatura menor que a do ambiente. Desconsiderado.\n\n");
    }
}

void shut_down() {
    while(turn_off(FAN));
    while(turn_off(RESISTOR));
    fclose(logger);
    exit(0);
}

void routine() {
    alarm(1);

    double pid;
    time_t rawtime;
    struct tm *info;
    char datetime[80];
    time(&rawtime);
    info = localtime(&rawtime);
    strftime(datetime, 80, "%d/%m/%Y %X", info);

    count = count % 2;

    do {
    ti = get_temperature(INTERNAL);
    } while (ti == -1.0);

    if (type == POTENCIOMETER) {
        do {
        tr = get_temperature(REFERENCE);
        } while (tr == -1.0);
    } else {
        tr = tc;
    }

    te = get_external_temperature();

    pid_set_constants(5.0, 1.0, 5.0);
    pid_set_reference(tr);
    pid = pid_control(ti);

    // Write temperatures on LCD
    write_temperatures(ti, tr, te);

    if(pid > 0) {
        while (control(RESISTOR, (int)pid));
        while (turn_off(FAN));
    } else {
        if(pid < -40.0) {
            while (control(FAN, (int)(-1.0*pid)));
        }
        while(turn_off(RESISTOR));
    }

    printf("TI: %.2f - TR: %.2f - TE: %.2f\n", ti, tr, te);
    if (count == 0) {
        fprintf(logger, "%s, %.2f, %.2f, %.2f, %.2f\n", datetime, ti, te, tr, pid);
    }

    count++;
}

int main() {
    logger = fopen("logger.csv", "w+");
    fprintf(logger, "Data/Hora, TI, TE, TR, PID\n");

    pthread_t id;

    while(setup_lcd());

    signal(SIGALRM, routine);
    signal(SIGINT, shut_down);

    pthread_create(&id, NULL, input, NULL);
    routine();

    while (1);

    return 0;
}
