#include <stdlib.h>
#include <string.h>
#include "uart.h"
#include "crc.h"

int open_uart() {
    return open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY);
}

void set_attributes(int uart_filestream) {
    struct termios options;
    tcgetattr(uart_filestream, &options);
    options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;
    tcflush(uart_filestream, TCIFLUSH);
    tcsetattr(uart_filestream, TCSANOW, &options);
}

void send_request(int uart_filestream, unsigned char code) {
    short crc;
    unsigned char tx_buffer[10];
    unsigned char *p_tx_buffer;
    p_tx_buffer = &tx_buffer[0];

    *p_tx_buffer++ = DISP;
    *p_tx_buffer++ = 0x23;
    *p_tx_buffer++ = code;
    *p_tx_buffer++ = 0x03;
    *p_tx_buffer++ = 0x06;
    *p_tx_buffer++ = 0x04;
    *p_tx_buffer++ = 0x06;
    crc = get_CRC(tx_buffer, 7);
    memcpy(p_tx_buffer, &crc, 2);
    p_tx_buffer += 2;

    write(uart_filestream, &tx_buffer[0], (p_tx_buffer - &tx_buffer[0]));
}

void read_response(int uart_filestream, unsigned char* rx_buffer) {
    int rx_length = read(uart_filestream, (void*)rx_buffer, 10);
    rx_buffer[rx_length] = '\0';
}

float get_temperature(unsigned char code) {
    int uart_filestream = -1;
    unsigned char rx_buffer[10];
    float temperature;
    short crc, check_crc;

    uart_filestream = open_uart();
    if (uart_filestream == -1) {
        return -1.0;
    }

    set_attributes(uart_filestream);
    send_request(uart_filestream, code);
    usleep(100000);
    read_response(uart_filestream, &rx_buffer[0]);

    memcpy(&temperature, &rx_buffer[3], 4);
    memcpy(&crc, &rx_buffer[7], 2);
    check_crc = get_CRC(&rx_buffer[0], 7);

    if (rx_buffer[0] != 0x00 || rx_buffer[1] != 0x23 || rx_buffer[2] != code || crc != check_crc)
        return -1.0;
    close(uart_filestream);

    return temperature;
}
