#include "pid.h"
#include <stdio.h>

double temperature, pid;
double reference = 0.0;
double Kp = 0.0;  // Ganho Proporcional
double Ki = 0.0;  // Ganho Integral
double Kd = 0.0;  // Ganho Derivativo
int T = 1;      // Período de Amostragem (ms)
unsigned long last_time;
double total_error = 0.0, prev_error = 0.0;
int max_signal = 100.0;
int min_signal = -100.0;

void pid_set_constants(double Kp_, double Ki_, double Kd_){
    Kp = Kp_;
    Ki = Ki_;
    Kd = Kd_;
}

void pid_set_reference(float reference_){
    reference = (double) reference_;
}

double pid_control(double temperature){

    double error = reference - temperature;

    total_error += error;

    if (total_error >= max_signal) 
    {
        total_error = max_signal;
    }
    else if (total_error <= min_signal) 
    {
        total_error = min_signal;
    }

    double delta_error = error - prev_error;

    pid = Kp*error + (Ki*T)*total_error + (Kd/T)*delta_error;

    if (pid >= max_signal) 
    {
        pid = max_signal;
    }
    else if (pid <= min_signal) 
    {
        pid = min_signal;
    }

    prev_error = error;

    return pid;
}
