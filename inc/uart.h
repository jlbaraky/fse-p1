#ifndef UART_H
#define UART_H

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

#define DISP 0x01
#define INTERNAL 0xC1
#define REFERENCE 0xC2

int open_uart();
void set_attributes(int);
void send_request(int, unsigned char);
void read_response(int, unsigned char*);
float get_temperature(unsigned char);

#endif
